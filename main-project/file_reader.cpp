#include "file_reader.h"
#include "constants.h"

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

date convert(const std::string& str) {
    date result;
    std::istringstream ss(str);
    char dot;
    ss >> result.day >> dot >> result.month >> dot >> result.year;

    if (ss.fail() || dot != '.') {
        std::cerr << "Incorrect date format!\n";
        exit(EXIT_FAILURE);
    }

    return result;
}


void read(const char* file_name, session_result* array[], int& size) {
    std::ifstream file(file_name);
    if (file.is_open()) {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (file.getline(tmp_buffer, MAX_STRING_SIZE)) {
            std::istringstream ss(tmp_buffer);
            session_result* result = new session_result;

            ss >> result->student.last_name;
            ss >> result->student.first_name;
            ss >> result->student.middle_name;

            std::string date_str;
            ss >> date_str;
            result->examDate = convert(date_str);

            ss >> result->mark;
            ss.get(); // To consume the space before the discipline
            ss.getline(result->discipline, MAX_STRING_SIZE);
            std::cout << "\n" << result->discipline << ".";

            array[size++] = result;
        }
        file.close();
    }
    else {
        throw "  ";
    }
}